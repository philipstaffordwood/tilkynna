/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.search;

/**
 * @author melissa
 *
 */
public class StatusFilterHelper {

    private StatusFilterHelper() {

    }

    /**
     * @param filterStatus
     * @return
     */
    public static Boolean getFilterStatusField(String filterStatus) {
        switch (filterStatus) {
        case "active":
            return Boolean.TRUE;
        case "inactive":
            return Boolean.FALSE;
        default:
            return Boolean.TRUE;
        }
    }
}
