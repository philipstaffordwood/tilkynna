/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.search;

import java.util.List;

import javax.persistence.criteria.Join;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;
import org.tilkynna.report.templates.TemplateEntity;
import org.tilkynna.report.templates.model.db.TemplateTagEntity;

/**
 * @author melissa
 *
 */
public class TemplateSpecBuilder extends BaseSpecification {

    private TemplateSpecBuilder() {

    }

    public static Specification<TemplateEntity> templateSearchSpec(String name, List<String> filterTags) {
        Specification<TemplateEntity> nameSpec = TemplateSpecBuilder.nameContains(name);
        Specification<TemplateEntity> tagsSpec = TemplateSpecBuilder.tags(filterTags);

        return Specification //
                .where(nameSpec) //
                .and(tagsSpec) //
        ;
    }

    public static Specification<TemplateEntity> nameContains(String name) {
        if (StringUtils.isEmpty(name)) {
            return null;
        }

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), getContainsLikePattern(name));
    }

    public static Specification<TemplateEntity> tags(List<String> tags) {
        if (tags == null || tags.isEmpty()) {
            return null;
        }

        Specification<TemplateEntity> specification = null;
        // Only find documents where ALL (and operator) filter tags are present.
        for (int i = 0; i < tags.size(); i++) {
            Specification<TemplateEntity> tagSpecification = Specification.where(TemplateSpecBuilder.tag(tags.get(i)));

            if (specification == null) {
                specification = tagSpecification;
            } else {
                specification = specification.and(tagSpecification);
            }
        }

        return specification;
    }

    public static Specification<TemplateEntity> tag(String tag) {
        // Oddly, when no filterTags data is set, Spring creates a single element array
        // with "new ArrayList<>()" as its value.
        // Check for this, as it indicates that the predicate should not be created.
        if (tag == null || tag.isEmpty() || tag.equals("new ArrayList<>()")) {
            return null;
        }

        return (root, criteriaQuery, criteriaBuilder) -> {
            Join<TemplateEntity, TemplateTagEntity> templateTagsJoin = root.join("templateTags");

            return criteriaBuilder.equal(criteriaBuilder.lower(templateTagsJoin.get("id").get("tag")), tag.toLowerCase());
        };
    }
}
