/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.search;

public abstract class BaseSpecification {

    protected BaseSpecification() {

    }

    public static final String WILDCARD = "%";

    protected static String getContainsLikePattern(String searchTerm) {
        if (searchTerm == null || searchTerm.isEmpty()) {
            return WILDCARD;
        } else {
            return WILDCARD + searchTerm.toLowerCase() + WILDCARD;
        }
    }
}
