/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.generate.model.db;

import java.time.ZonedDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.UpdateTimestamp;
import org.tilkynna.common.hibernate.PostgreSQLEnumType;
import org.tilkynna.report.callback.GenerateReportCallbackStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "callback")
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
public class CallbackEntity {

    @Id
    @Column(name = "id", columnDefinition = "SMALLINT")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Short id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "correlation_id")
    private GeneratedReportEntity generatedReport;

    @Enumerated(EnumType.STRING)
    @Column(name = "callback_status", nullable = false, updatable = true)
    @Type(type = "pgsql_enum")
    private GenerateReportCallbackStatus callbackStatus;

    @Column(name = "last_updated")
    @UpdateTimestamp
    private ZonedDateTime lastUpdated;

}
