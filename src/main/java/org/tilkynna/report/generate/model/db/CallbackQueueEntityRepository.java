/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
/**
 * *************************************************
CallbackQueueEntity * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.generate.model.db;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CallbackQueueEntityRepository extends JpaRepository<CallbackQueueEntity, UUID>, JpaSpecificationExecutor<CallbackQueueEntity> {

    @Query(value = " UPDATE _reports.callback_queue cb1 SET retries_remaining = retries_remaining - 1, last_updated = now() " + //
            " WHERE cb1.correlation_id = (  " + //
            "    SELECT cb2.correlation_id FROM _reports.callback_queue cb2  " + //
            "    WHERE cb2.retries_remaining > 0 and now() - cast(CONCAT(:backOfIntervalInMilliseconds, 'milliseconds') AS interval) >= last_updated" + //
            "    ORDER BY cb2.last_updated FOR UPDATE SKIP LOCKED LIMIT 1 " + //
            " )" + //
            " RETURNING * " //
            , nativeQuery = true)
    public CallbackQueueEntity findCallbackQueueEntity(@Param("backOfIntervalInMilliseconds") int backOfIntervalInMilliseconds);

}
