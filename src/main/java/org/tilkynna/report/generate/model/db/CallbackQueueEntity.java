/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.generate.model.db;

import java.time.ZonedDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.UpdateTimestamp;
import org.tilkynna.common.hibernate.PostgreSQLEnumType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "callback_queue")
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
public class CallbackQueueEntity {

    @Id
    @Column(name = "correlation_id")
    private UUID correlationId;

    @Enumerated(EnumType.STRING)
    @Column(name = "report_status", nullable = false)
    @Type(type = "pgsql_enum")
    private ReportStatusEntity reportStatus;

    @Column(name = "callback_url")
    @Type(type = "org.hibernate.type.TextType")
    private String callbackUrl;

    @Column(name = "retries_remaining", columnDefinition = "SMALLINT", updatable = true)
    private Short retriesRemaining;

    @Column(name = "created_at")
    @CreationTimestamp
    private ZonedDateTime createdAt;

    @Column(name = "last_updated")
    @UpdateTimestamp
    private ZonedDateTime lastUpdated;

}
