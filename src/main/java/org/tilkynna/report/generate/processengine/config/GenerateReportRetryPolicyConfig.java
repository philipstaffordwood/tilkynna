/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.generate.processengine.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
public class GenerateReportRetryPolicyConfig {

    /**
     * The maximum number of times to retry a failed report generation
     */
    @Value("${tilkynna.generate.retry.maxAttempts}")
    short maxAttempts = 3;

    /**
     * The time in milliseconds to delay next retry since last failure
     */
    @Value("${tilkynna.generate.retry.backOffPeriodInMilliseconds:234}")
    short backOffPeriod = 5000;
}
