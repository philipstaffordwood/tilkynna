/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.destination.provider;

import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.tilkynna.ReportingConstants;
import org.tilkynna.common.error.ResourceNotFoundExceptions;
import org.tilkynna.common.utils.PaginatedResultsRetrievedEventListener;
import org.tilkynna.report.destination.model.dao.DestinationEntityRepository;
import org.tilkynna.report.destination.model.db.DestinationEntity;
import org.tilkynna.report.destination.model.db.SMTPDestinationEntity;
import org.tilkynna.report.destination.model.db.SelectedDestinationParameterEntity;
import org.tilkynna.report.generate.model.db.GeneratedReportEntity;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service(ReportingConstants.SMTP)
public class SmtpDestinationProvider implements DestinationProvider {

    @Autowired
    private DestinationEntityRepository destinationRepository;

    private DestinationEntity getDestinationEntity(UUID destinationId) {
        return destinationRepository.findById(destinationId) //
                .orElseThrow(() -> new ResourceNotFoundExceptions.Destination(destinationId.toString()));
    }

    private JavaMailSender getJavaMailSender(SMTPDestinationEntity smtp) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(smtp.getHost());
        mailSender.setPort(smtp.getPort());

        mailSender.setUsername(smtp.getUsername());
        mailSender.setPassword(new String(smtp.getPassword()));

        Properties props = mailSender.getJavaMailProperties();
        // props.put("mail.transport.protocol", "smtp");
        // props.put("mail.smtp.auth", "true");
        // props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    @Override
    public void write(GeneratedReportEntity reportRequest, byte[] reportFile) throws IOException, MessagingException {
        UUID destinationId = reportRequest.getDestination().getDestinationId();
        SMTPDestinationEntity smtpDestination = (SMTPDestinationEntity) getDestinationEntity(destinationId);

        Set<SelectedDestinationParameterEntity> selectedDestinationParameters = reportRequest.getSelectedDestinationParameters();

        JavaMailSender emailSender = getJavaMailSender(smtpDestination);
        MimeMessage message = emailSender.createMimeMessage();

        String attachementName = reportRequest.getCorrelationId() + "." + reportRequest.getExportFormat().getName().toLowerCase();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.addAttachment(attachementName, new ByteArrayResource(reportFile));
        helper.setFrom(smtpDestination.getFromAddress());
        selectedDestinationParameters.forEach(selectedParam -> {
            String name = selectedParam.getDestinationParameter().getName();
            String value = selectedParam.getValue();

            try {
                switch (name) {
                case "to":
                    helper.setTo(value);
                    break;
                case "cc":
                    helper.setCc(value);
                    break;
                case "bcc":
                    helper.setBcc(value);
                    break;
                case "subject":
                    helper.setSubject(value);
                    break;
                case "body":
                    helper.setText(value);
                    break;
                default:
                    // no match
                }
            } catch (MessagingException e) {
                log.error(e.getMessage());
            }

        });

        emailSender.send(message);
    }

    @Override
    public boolean testConnection(DestinationEntity destination) {
        SMTPDestinationEntity smtpDestinationTest = (SMTPDestinationEntity) destination;
        JavaMailSenderImpl emailSender = (JavaMailSenderImpl) getJavaMailSender(smtpDestinationTest);
        try {
            emailSender.testConnection();
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}
