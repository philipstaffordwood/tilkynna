/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.destination.model.dao;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;
import org.tilkynna.report.destination.model.db.DestinationEntity;
import org.tilkynna.report.search.BaseSpecification;

public class DestinationEntitySpecBuilder extends BaseSpecification {

    private DestinationEntitySpecBuilder() {

    }

    public static Specification<DestinationEntity> destinationSearchSpec(String name, Boolean isActive) {
        Specification<DestinationEntity> nameSpec = DestinationEntitySpecBuilder.nameContains(name);
        Specification<DestinationEntity> activeSpec = DestinationEntitySpecBuilder.isActive(isActive);

        return Specification.where(activeSpec).and(nameSpec);
    }

    public static Specification<DestinationEntity> nameContains(String name) {
        if (StringUtils.isEmpty(name)) {
            return null;
        }

        return (destination, cq, cb) -> cb.like(cb.lower(destination.get("name")), getContainsLikePattern(name));
    }

    public static Specification<DestinationEntity> isActive(Boolean isActive) {
        if (isActive == null) {
            return null;
        }

        return (datasource, cq, cb) -> cb.equal(datasource.get("isActive"), isActive);
    }

}
