/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.destination;

import java.util.List;
import java.util.UUID;

import org.openapitools.api.DestinationsApi;
import org.openapitools.model.DestinationCreateBase;
import org.openapitools.model.DestinationResponseBase;
import org.openapitools.model.DestinationResponseHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import org.tilkynna.common.utils.PaginatedResultsRetrievedEvent;
import org.tilkynna.common.utils.ParseOrderByQueryParam;
import org.tilkynna.report.destination.model.dao.DestinationEntitySpecBuilder;
import org.tilkynna.report.destination.model.db.DestinationEntity;
import org.tilkynna.report.destination.strategy.DestinationEntityStrategy;
import org.tilkynna.report.destination.strategy.DestinationEntityStrategyFactory;
import org.tilkynna.report.search.StatusFilterHelper;

@RestController
@PreAuthorize("hasRole('TILKYNNA_USER') or hasRole('TILKYNNA_ADMIN')")
public class DestinationsAPIController implements DestinationsApi {
    private static final String DEFAULT_SORT_FIELD = "name";

    @Autowired
    private DestinationEntityStrategyFactory destinationStrategyFactory;

    @Autowired
    private DestinationService destinationService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    public ResponseEntity<DestinationResponseBase> addReportDestination(DestinationCreateBase destinationCreateBase) {
        DestinationEntityStrategy createDestinationStrategy = destinationStrategyFactory.createStrategy(destinationCreateBase);
        DestinationResponseBase destinationResponseBase = destinationService.createDestination(createDestinationStrategy);

        return new ResponseEntity<>(destinationResponseBase, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<DestinationResponseHeader>> listConfiguredDestinations(Integer page, Integer size, String filterName, String filterStatus, List<String> orderBy) {
        Sort sort = ParseOrderByQueryParam.resolveArgument(orderBy, DEFAULT_SORT_FIELD);
        final PageRequest pr = PageRequest.of(page, size, sort);

        Boolean filterStatusOption = StatusFilterHelper.getFilterStatusField(filterStatus);
        Specification<DestinationEntity> search = DestinationEntitySpecBuilder.destinationSearchSpec(filterName, filterStatusOption);
        Page<DestinationResponseHeader> destinationsHeaders = destinationService.findAll(search, pr);

        if (destinationsHeaders.getContent().isEmpty()) {
            return new ResponseEntity<>(destinationsHeaders.getContent(), HttpStatus.NO_CONTENT);
        } else {
            HttpHeaders headers = new HttpHeaders();
            eventPublisher.publishEvent(new PaginatedResultsRetrievedEvent<>(this, destinationsHeaders, headers));

            return new ResponseEntity<>(destinationsHeaders.getContent(), headers, HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<Void> validateDestination(UUID destinationId) {
        destinationService.validateConnection(destinationId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> inactivateReportDestination(UUID destinationId) {
        destinationService.inactivateDestination(destinationId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DestinationResponseBase> getDestination(UUID destinationId) {
        return new ResponseEntity<>(destinationService.getDestination(destinationId), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateDestination(UUID destinationId, DestinationCreateBase destinationCreateBase) {
        DestinationEntityStrategy createDestinationStrategy = destinationStrategyFactory.createStrategy(destinationCreateBase);
        destinationService.updateDestination(destinationId, createDestinationStrategy);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

}
