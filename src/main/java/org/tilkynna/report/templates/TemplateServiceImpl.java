/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.templates;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.openapitools.model.LookupTag;
import org.openapitools.model.Template;
import org.openapitools.model.TemplateDetail;
import org.openapitools.model.TemplateTagList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.tilkynna.ReportingConstants;
import org.tilkynna.common.error.AlreadyExistsExceptions;
import org.tilkynna.common.error.CustomValidationExceptions;
import org.tilkynna.common.error.ResourceNotFoundExceptions;
import org.tilkynna.common.error.TemplateEngineExceptions;
import org.tilkynna.common.storage.ContentRepository;
import org.tilkynna.engine.TemplateEngine;
import org.tilkynna.engine.TemplateEngineFactory;
import org.tilkynna.engine.model.TemplateEngineParameter;
import org.tilkynna.lookup.tag.LookupTagRepository;
import org.tilkynna.report.datasource.model.dao.DatasourceEntityRepository;
import org.tilkynna.report.datasource.model.db.DatasourceEntity;
import org.tilkynna.report.templates.assembler.TemplateAssembler;
import org.tilkynna.report.templates.assembler.TemplateParametersAssembler;
import org.tilkynna.report.templates.model.db.TemplateTagEntity;

import lombok.extern.slf4j.Slf4j;

//TODO rollback .. if 1 the below things fail?

// TODO -- validation needed: can I validate that file is XML (or BIRT xml even)?
@Service
@Slf4j
public class TemplateServiceImpl implements TemplateService {

    @Autowired
    private ContentRepository contentRepository;

    @Autowired
    private TemplateEntityRepository templateRepository;

    @Autowired
    private LookupTagRepository lookupTagRepository;

    @Autowired
    private DatasourceEntityRepository datasourceEntityRepository;

    @Autowired
    private TemplateAssembler templateAssembler;

    @Autowired
    private TemplateEngineFactory templateEngineFactory;

    @Autowired
    private TemplateParametersAssembler templateParamsAssembler;

    private final Path rootLocation;

    @Autowired
    public TemplateServiceImpl(TemplateStorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public Template save(TemplateRequest templateRequest) {
        if (templateRepository.existsByNameIgnoreCase(templateRequest.getTemplateName())) {
            throw new AlreadyExistsExceptions.Template(templateRequest.getTemplateName());
        }
        String fileExtension = ReportingConstants.extractFileExtension(templateRequest.getFile().getOriginalFilename());
        if (!ReportingConstants.VALID_FILE_EXTENSIONS.contains(fileExtension)) {
            throw new CustomValidationExceptions.TemplateFileExtensionNotAllowedException(fileExtension);
        }
        validateTemplateRequest(templateRequest);

        TemplateEntity template = saveTemplateToDB(templateRequest);
        saveTemplateToStorage(templateRequest.getFile(), template.getId());

        return templateAssembler.mapTemplateEntityToTemplate(template);
    }

    @Override
    public Template update(UUID templateId, TemplateRequest templateRequest) {
        Optional<TemplateEntity> existingTemplateOpt = templateRepository.findById(templateId);

        if (existingTemplateOpt.isPresent()) {
            TemplateEntity existingTemplateEntity = existingTemplateOpt.get();

            populateMissingTemplateRequestFromExistingTemplate(templateRequest, existingTemplateEntity);

            boolean nameChanged = !templateRequest.getTemplateName().equalsIgnoreCase(existingTemplateEntity.getName());
            if (nameChanged) {
                TemplateEntity templateEntityByName = templateRepository.findByNameIgnoreCase(templateRequest.getTemplateName());

                if (templateEntityByName != null && !templateEntityByName.getId().equals(existingTemplateEntity.getId())) {
                    throw new AlreadyExistsExceptions.Template(templateRequest.getTemplateName());
                }
            }
            validateTemplateRequest(templateRequest);

            TemplateEntity template = updateTemplateToDB(existingTemplateEntity, templateRequest);
            if (templateRequest.getFile() != null) {
                String fileExtension = ReportingConstants.extractFileExtension(templateRequest.getFile().getOriginalFilename());
                if (!ReportingConstants.VALID_FILE_EXTENSIONS.contains(fileExtension)) {
                    throw new CustomValidationExceptions.TemplateFileExtensionNotAllowedException(fileExtension);
                }

                saveTemplateToStorage(templateRequest.getFile(), template.getId());
            }

            return templateAssembler.mapTemplateEntityToTemplate(template);
        } else {
            throw new ResourceNotFoundExceptions.Template(templateId.toString());
        }
    }

    /**
     * @param templateRequest
     * @param existingTemplateEntity
     */
    private void populateMissingTemplateRequestFromExistingTemplate(TemplateRequest templateRequest, TemplateEntity existingTemplateEntity) {
        if (templateRequest.getDatasourceIds() == null) {
            Set<DatasourceEntity> datasources = existingTemplateEntity.getDatasources();

            List<UUID> datasourceIds = new ArrayList<>();
            datasources.forEach(d -> datasourceIds.add(d.getId()));
            templateRequest.setDatasourceIds(datasourceIds);
        }

        if (templateRequest.getTags() == null) {
            List<TemplateTagEntity> templateTags = existingTemplateEntity.getTemplateTags();

            List<String> tags = new ArrayList<>();
            templateTags.forEach(tag -> tags.add(tag.getId().getTag()));
            templateRequest.setTags(tags);
        }
    }

    private TemplateEntity updateTemplateToDB(TemplateEntity existingTemplate, TemplateRequest templateRequest) {
        Set<DatasourceEntity> datasourceEntities = gatherDatasourceEntities(templateRequest.getDatasourceIds());

        if (templateRequest.getFile() != null) {
            existingTemplate.setOriginalFilename(templateRequest.getFile().getOriginalFilename());
        }
        existingTemplate.setName(templateRequest.getTemplateName());
        existingTemplate.setDatasources(datasourceEntities);
        existingTemplate.removeAllTags(); // 1st remove any existing
        existingTemplate.addTags(templateRequest.getTags()); // now add all back

        return templateRepository.save(existingTemplate);
    }

    private void validateTemplateRequest(TemplateRequest templateRequest) {
        if (templateRequest == null || templateRequest.getTemplateName() == null) {
            throw new TemplateNameNotEmptyException();
        }

        if (onlyAllowOneDatasourcePerTemplate(templateRequest)) {
            throw new CustomValidationExceptions.OneDatasourcePerTemplate();
        }

        List<UUID> datasources = templateRequest.getDatasourceIds();
        if (datasources == null || datasources.isEmpty()) {
            throw new TemplateEngineExceptions.TempalteHasNoDatasourcesException(templateRequest.getTemplateName());
        }

        UUID datasourceId = datasources.get(0);
        if (!datasourceEntityRepository.existsById(datasourceId)) {
            throw new ResourceNotFoundExceptions.Datasource(datasourceId.toString());
        }

    }

    /**
     * Currently we only allowing for 1 datasrouce per template. As the functionality for: </br>
     * - validating all datasources are active </br>
     * - getting BIRT engine to use all the datasources </br>
     * is going to take more time. For first MVP only 1 datasource is needed for now.
     * 
     * @param templateRequest
     * @return true/false datasourceId's has 1 and only 1 element
     */
    private boolean onlyAllowOneDatasourcePerTemplate(TemplateRequest templateRequest) {
        // templateRequest.getDatasourceIds() will never be empty as its validated for before entering into service class
        return templateRequest.getDatasourceIds().size() > 1;
    }

    private void saveTemplateToStorage(MultipartFile templateFile, UUID uuID) {
        contentRepository.store(templateFile, uuID);
    }

    // https://www.postgresql.org/docs/current/ddl-inherit.html#DDL-INHERIT-CAVEATS
    private TemplateEntity saveTemplateToDB(TemplateRequest templateRequest) {
        UUID dsId = templateRequest.getDatasourceIds().get(0); // templateRequest.getDatasourceIds() can never be null at this point
        DatasourceEntity existingDatasource = datasourceEntityRepository.findById(dsId).orElseThrow(() -> new ResourceNotFoundExceptions.Datasource(dsId.toString()));

        TemplateEntity template = templateAssembler.mapTemplateRequestToTemplateEntity(templateRequest);
        template.addDatasource(existingDatasource);

        template = templateRepository.save(template);
        template.addTags(templateRequest.getTags());
        template = templateRepository.save(template);

        return template;
    }

    @Override
    public Page<Template> findAll(Specification<TemplateEntity> search, Pageable pageable) {
        Page<TemplateEntity> templatesDB = templateRepository.findAll(search, pageable);

        return templateAssembler.mapListTemplateEntityToTemplates(templatesDB);
    }

    @Override
    public TemplateTagList addTemplateTags(UUID templateId, List<LookupTag> lookupTags) {
        TemplateEntity templateDB = templateRepository.findById(templateId) //
                .orElseThrow(() -> new ResourceNotFoundExceptions.Template(templateId.toString()));

        lookupTags.forEach(lookupTagElement -> templateDB.addTag(lookupTagElement.getTag()));
        templateRepository.save(templateDB);

        return templateAssembler.mapListStringToTemplateTagList(templateId, lookupTags);
    }

    @Override
    public void removeTempalteTags(UUID templateId) {
        TemplateEntity templateEntity = templateRepository.findById(templateId) //
                .orElseThrow(() -> new ResourceNotFoundExceptions.Template(templateId.toString()));

        templateEntity.removeAllTags();

        templateRepository.save(templateEntity);
    }

    @Override
    public Set<DatasourceEntity> gatherDatasourceEntities(List<UUID> datasourceIds) {
        Set<DatasourceEntity> datasources = new HashSet<>();
        datasourceIds.forEach(datasourceId -> {
            DatasourceEntity datasourceEntity = datasourceEntityRepository.findById(datasourceId)//
                    .orElseThrow(() -> new ResourceNotFoundExceptions.Datasource(datasourceId.toString()));
            datasources.add(datasourceEntity);
        });

        return datasources;
    }

    @Override
    public TemplateDetail getTemplateDetail(UUID templateId) {
        TemplateEntity templateHeaderDb = templateRepository.findById(templateId).orElseThrow(() -> new ResourceNotFoundExceptions.Template(templateId.toString()));

        TemplateDetail templateDetail = new TemplateDetail();
        templateDetail.setHeader(templateAssembler.mapTemplateEntityToTemplate(templateHeaderDb));
        templateDetail.setParameters(templateParamsAssembler.templateEngineParameterToTemplateParameter(getTemplateParameters(templateId, templateHeaderDb)));

        return templateDetail;
    }

    private List<TemplateEngineParameter> getTemplateParameters(UUID templateId, TemplateEntity templateHeaderDb) {
        TemplateEngine templateEngine = templateEngineFactory.getReportService(templateHeaderDb.getOriginalFilename());
        Path pathToStoredFile = this.rootLocation.resolve(templateId.toString());

        return templateEngine.getTemplateParameters(templateId.toString(), pathToStoredFile.toString());

    }

}
