/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.callback;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

// https://medium.com/@guraycintir/task-scheduling-in-spring-boot-e70a1069a9f
@Component
public class GenerateReportCallbackScheduler {

    @Autowired
    private GenerateReportCallbackHandler callbackHandler;

    @Scheduled(fixedRateString = "1000", initialDelayString = "1000")
    public void startWorkerThread() {
        callbackHandler.run();
    }
}
