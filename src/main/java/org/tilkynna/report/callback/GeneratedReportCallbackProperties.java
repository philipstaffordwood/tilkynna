/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.callback;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties("tilkynna.generatereportcallback")
public class GeneratedReportCallbackProperties {

    short maxAttempts = 3;

    short backOffPeriodInMilliseconds = 5000;

}
