/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.callback;

import org.openapitools.model.ReportGenerationEvent;
import org.openapitools.model.ReportGenerationEvent.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.tilkynna.report.generate.model.db.CallbackEntity;
import org.tilkynna.report.generate.model.db.CallbackEntityRepository;
import org.tilkynna.report.generate.model.db.CallbackQueueEntity;
import org.tilkynna.report.generate.model.db.CallbackQueueEntityRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class GenerateReportCallbackHandler {

    @Autowired
    private CallbackEntityRepository callbackEntityRepository;

    @Autowired
    private CallbackQueueEntityRepository callbackQueueEntityRepository;

    @Autowired
    private GeneratedReportCallbackProperties generatedReportCallbackProperties;

    @Async("generateReportCallbackThreadPool")
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void run() {
        log.debug("worker thread started. current thread name {}", Thread.currentThread().getName());

        CallbackQueueEntity callbackQueueItem = callbackQueueEntityRepository.findCallbackQueueEntity(generatedReportCallbackProperties.getBackOffPeriodInMilliseconds());

        if (callbackQueueItem != null) {
            CallbackEntity callback = callbackEntityRepository.findByGeneratedReportCorrelationId(callbackQueueItem.getCorrelationId());
            GenerateReportCallbackStatus callbackStatus = GenerateReportCallbackStatus.DONE;

            try {
                ReportGenerationEvent reportGenerationEvent = new ReportGenerationEvent();
                reportGenerationEvent.setCorrelationId(callbackQueueItem.getCorrelationId());
                reportGenerationEvent.setStatus(StatusEnum.valueOf(callbackQueueItem.getReportStatus().name()));

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.postForObject(callbackQueueItem.getCallbackUrl(), reportGenerationEvent, HttpStatus.class);

            } catch (RestClientException e) {
                if (callbackQueueItem.getRetriesRemaining() != 0) {
                    callbackQueueEntityRepository.save(callbackQueueItem);
                }
                callbackStatus = GenerateReportCallbackStatus.FAILED;
            }

            boolean isCallbackTaskFinished = //
                    GenerateReportCallbackStatus.DONE.equals(callbackStatus) || (callbackQueueItem.getRetriesRemaining() == 0);
            if (isCallbackTaskFinished) {
                callback.setCallbackStatus(callbackStatus);
                callbackEntityRepository.save(callback);

                callbackQueueEntityRepository.delete(callbackQueueItem);
            }
        }

        log.debug("worker thread finished. current thread name {}", Thread.currentThread().getName());
    }
}
