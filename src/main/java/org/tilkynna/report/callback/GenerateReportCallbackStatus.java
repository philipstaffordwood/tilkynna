/**
 * *************************************************
 * Copyright (c) 2019, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package org.tilkynna.report.callback;

public enum GenerateReportCallbackStatus {
    WAITING, DONE, FAILED;
}
