ALTER TABLE _reports.generated_report ADD COLUMN callback_url text;

-- object: _reports.callback | type: TABLE --
-- DROP TABLE IF EXISTS _reports.callback CASCADE;
CREATE TABLE _reports.callback (
	id smallint NOT NULL GENERATED ALWAYS AS IDENTITY ,
	correlation_id uuid,
	callback_status text NOT NULL,
	last_updated timestamptz NOT NULL,
	CONSTRAINT callback_pk PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON COLUMN _reports.callback.callback_status IS 'WAITING - for generate report request to complete (pass or fail) 
DONE
FAILED';
-- ddl-end --
ALTER TABLE _reports.callback OWNER TO postgres;
-- ddl-end --




-- object: _reports.callback_queue | type: TABLE --
-- DROP TABLE IF EXISTS _reports.callback_queue CASCADE;
CREATE TABLE _reports.callback_queue (
	correlation_id uuid NOT NULL,
	report_status _reports.report_status NOT NULL,
	callback_url text NOT NULL,
	retries_remaining smallint NOT NULL DEFAULT 3,
	created_at timestamptz NOT NULL,
	last_updated timestamptz,
	CONSTRAINT un_correlation_id UNIQUE (correlation_id),
	CONSTRAINT callback_queue_pk PRIMARY KEY (correlation_id)

);
-- ddl-end --
COMMENT ON COLUMN _reports.callback_queue.retries_remaining IS 'Number of times left to rety upon failure ';
-- ddl-end --
ALTER TABLE _reports.callback_queue OWNER TO postgres;
-- ddl-end --


