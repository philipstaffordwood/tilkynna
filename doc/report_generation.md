# Report Generation Functionality

Tilkynna exposes 2 endpoints which relate to the generation of a report, these are:
- `/templates/{templateId}/generate` which requests for a report to be generated for a given templateId
- `/reports/{correlationId}/status` gives you the status of you report generation request above. Where correlationId is the response from the first call.

A requested report generation can move through the following statuses. 
- **PENDING**: Your request for a report has been received but no processing of the report has started yet. 
- **STARTED**: Generation of the report has started and is currently in progress.
- **FAILED**: Something has gone wrong with the generation of the report, try again or contact admin. 
- **FINISHED**: Generation of report has finished, and for a request made to a downloadable destination you can download this report 

![](images/Tilkynna_GenerateReportRequest_StateDiagram.jpeg)
 

## Tilkynna uses an internal queue, off which to process report generation requests. 
This has been implemented using Postgres's `SELECT ... FOR UPDATE SKIP LOCKED` feature. 
  - Basically: `With SKIP LOCKED, any selected rows that cannot be immediately locked are skipped.`
  - More details on Postres's SKIP LOCKED at: https://www.postgresql.org/docs/current/sql-select.html

The table used is `generated_report` which holds all data related to report generation requests. 
This includes:
- details of what was requested for the report (when, who, what)
- and status of the request as described above. 

## On receiving a request to generated a report `/templates/{templateId}/generate` Tilkynna will process it as follows: 
![](images/Tilkynna-GenerateReportRequest.jpeg)

## Once a request is saved to the `generate_report` table:
- The `GenerateReportQueueScheduler` monitors the `generated_report` table  for 3 statuses, namely:
  - **PENDING**: to initiate the report generation process
  - **FAILED**: to check if an retry might be needed, and move report request to PENDING again
  - **STARTED**: to check if there are any long running reports 
- Any PENDING requests are pushed asynchronously onto the `GeneratedReport Task Thread Pool` 
  - which delegates actually report generation to a GenerateReportHandler 
- Any FAILED requests are checked for number of retries and if any existing, will requeue these to a PENDING status
- Any STARTED requests in this status for more than x hours, will requeue these to a PENDING status

The basics for each of the above processes is: 
- a scheduler is triggered every x milliseconds (based on application config as described below)
- the scheduler will run the job acquisition query looking for an report requests it needs to act against 
- each request found, will be pushed onto a thread pool which deals with the specifics for that report request (ie: pushing to sending to Generation Thread Pool or requeuing).

This process looks as follows: 
![](images/Tilkynna-ReportGeneration-Queue.jpeg)
  
- The schedule's for above monitors can be configured in your application.yaml 

  ```yaml
  tilkynna:
    ...
    generate: 
      ...
      monitorPendingRequests: 
        fixedRateInMilliseconds: 1000
        initialDelayInMilliseconds: 5000
      monitorFailedRequests: 
        fixedRateInMilliseconds: 1000
        initialDelayInMilliseconds: 5000
      monitorStalledRequests: 
        fixedRateInMilliseconds: 1000
        initialDelayInMilliseconds: 5000
  ```

  Where:
    **fixedRateInMilliseconds:** is always the fixed period in milliseconds between invocations for a specified monitor's schedule. 

    **initialDelayInMilliseconds:** is always the time milliseconds to delay before the first execution of for a specified monitor's schedule after server startup.

  and Where: 
    **monitorPendingRequests:** relates to the scheduler for enquiring pending report requests

    **monitorFailedRequests:** relates to the scheduler for checking if failed requests need to be retried or not

    **monitorStalledRequests:**  relate to the scheduler for checking if there are any requests stuck in the STARTED status 

- The number of retries which the server will do can also be  configured in your application.yaml

  ```yaml
  tilkynna:
    generate: 
      retry: 
        maxAttempts: 3
        backOffPeriodInMilliseconds: 5000
  ```

  Where:
     **maxAttempts:**  is the number of retries that will be done
     **backOffPeriodInMilliseconds:** is the amount of time in milliseconds that the server will wait before attempting the report generation again on one that previously failed. 

