# Tilknna ERD

![](tilkynna.png)

The DB model (ERD) was created with pgmodeler 
- the model files (.dbm) can be opened using pgmodeler in docker container which can be found at https://hub.docker.com/r/grindrodbank/pgmodeler.

## Instructions for Development

The following commands will have you up and running in no time:
```text
docker pull grindrodbank/pgmodeler

docker run --rm -it --user $(id -u) -e DISPLAY=unix$DISPLAY --workdir=$(pwd) --volume="/home/$USER:/home/$USER" --volume="/etc/group:/etc/group:ro" --volume="/etc/passwd:/etc/passwd:ro" --volume="/etc/shadow:/etc/shadow:ro" --volume="/etc/sudoers.d:/etc/sudoers.d:ro" -v /tmp/.X11-unix:/tmp/.X11-unix grindrodbank/pgmodeler
```

You can run a docker container with postgres to export the model to using: 
```bash
docker run -d --name tilkynnadb -p 5432:5432 -e POSTGRES_PASSWORD=postgres postgres:11-alpine
```

Get the IP of the docker container running above (to use as a connection string on pgmodeler):
```bash 
docker inspect -f '{{ range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' tilkynnadb`
```	

You can SSH onto the docker container using: 
```bash
docker exec -it tilkynnadb sh

psql -h localhost -U postgres 

\c tilkynna
```

## Instructions getting SQL needed, to include in Flyway scripts etc.
- You can export via pg_dump E.g.:
`docker exec -u postgres ocsdb pg_dump -d tilkynna --schema-only --schema=_entity --schema=_reports > tilkynna.sql`
